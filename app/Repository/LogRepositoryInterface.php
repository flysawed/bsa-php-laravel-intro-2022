<?php

namespace App\Repository;

interface LogRepositoryInterface
{
    public function findAll(): array;
}
